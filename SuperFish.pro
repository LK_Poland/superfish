TEMPLATE = app
QT       += core widgets charts

RC_ICONS = superfish.ico

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        superfish.cpp

FORMS += \
    mainwindow.ui

HEADERS += \
    mainwindow.h \
    superfish.h
