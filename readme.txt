Enigma style algorithm for text encryption.

Encryption process is performed by 15 rotors.
Each rotor has unique mapping for every letter of alphabet.

For instance mapping for first rotor is: pvgdwbqjfsthznuoicxeklymar
Which means a is mapped to p, b is mapped to v and so on...

Encryption key is 15 letters long and is used to shift each rotor from its initial position.
For instance if phrase "mypasswordisbad" is used as encryption key, first rotor mapping
is shifted by 'm' (109) positions, second rotor mapping is shifted by 'y' (121) position and so on.

During encryption process each plaintext letter is mapped to new value 15 times.
For instance if plaintext is "simpletext", rotor #1 can map letter 's' to 'q' and shift
itself by 1, rotor #2 can map letter 'q' to 'k' and shift itself by 2 and so on.

Plaintext letter can be mapped to the same letter of ciphertext.
This was not the case in Enigma encryption (security flaw).