#include "mainwindow.h"
#include "ui_mainwindow.h"

static const QString AppVersion = "1.1";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    std::cout << SuperFish::addRotorsFromFile() << " rotor(s) added from file." << std::endl;

    setWindowIcon(QIcon("superfish.ico"));

    connect(ui->actionOpen_rotors, SIGNAL(triggered()), this, SLOT(openRotors()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(helpAbout()));
    connect(ui->actionAbout_Qt, SIGNAL(triggered()), this, SLOT(helpAboutQt()));

    const QStringList& args = qApp->arguments();

    if (args.size() == 3)
    {
        ui->txtKey->setText(args[2]);
        ui->txtPlainText->setPlainText(readFile(args[1]));
    }

    on_rdoEncrypt_toggled(ui->rdoEncrypt->isChecked());

    histPlainText = createHistogram(ui->horizontalLayout_3, "Plaintext histogram",
                                    ui->txtPlainText->toPlainText(), &axisPlainText);
    histCipherText = createHistogram(ui->horizontalLayout_4, "Ciphertext histogram",
                                     ui->txtCipherText->toPlainText(), &axisCipherText);

    statusBar()->showMessage(
                QString("Encryption with %1 rotors and %2-bit key.")
                    .arg(SuperFish::rotorInit.size())
                    .arg(SuperFish::keyLengthInBits()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateHistogram(QChart* chart, const QString& text, QValueAxis *yAxis)
{
    if (nullptr == chart)
    {
        return;
    }
    const std::vector<unsigned int>& histogram = SuperFish::histogram(
                                                     SuperFish::normalize(text.toStdString()));
    QBarSeries *series = (QBarSeries*)chart->series().at(0);
    QBarSet* set = new QBarSet("");
    unsigned int max = 0;
    for (unsigned int val : histogram)
    {
        *set << val;
        max = std::max(max, val);
    }
    series->clear();
    series->append(set);
    chart->removeSeries(series);
    chart->addSeries(series);
    yAxis->setRange(0, max);
    yAxis->applyNiceNumbers();
}

QChart* MainWindow::createHistogram(QHBoxLayout* layout, const QString& title, const QString& text,
                                    QValueAxis **yAxis)
{
    QStringList categories;
    for (char c = 'a' ; c <= 'z' ; ++c)
    {
        categories << QString(1, c);
    }

    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);

    QValueAxis *axisY = new QValueAxis();
    axisY->setLabelFormat("%i");

    QBarSeries *series = new QBarSeries();
    series->append(new QBarSet(""));
    series->attachAxis(axisX);
    series->attachAxis(axisY);

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle(title);
    chart->setAnimationOptions(QChart::SeriesAnimations);
    chart->legend()->setVisible(false);
    chart->addAxis(axisX, Qt::AlignBottom);
    chart->addAxis(axisY, Qt::AlignLeft);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    layout->addWidget(chartView);

    updateHistogram(chart, text, axisY);

    *yAxis = axisY;
    return chart;
}

QString MainWindow::readFile(const QString& path)
{
    QFile fin(path);
    if (fin.open(QFile::ReadOnly))
    {
        QTextStream sin(&fin);
        return fin.readAll();
    }
    return "";
}

void MainWindow::writeFile(const QString& path, const QString& text)
{
    QFile fout(path);
    if (fout.open(QFile::WriteOnly))
    {
        QTextStream sout(&fout);
        sout << text;
    }
}

QString MainWindow::getSelectedFile()
{
    const QString& path = QFileDialog::getOpenFileName(this, "Open text file", QDir::currentPath(), "*.txt");
    return readFile(path);
}

void MainWindow::saveSelectedFile(const QString& text)
{
    const QString& path = QFileDialog::getSaveFileName(this, "Save text file", QDir::currentPath(), "*.txt");
    writeFile(path, text);
}

void MainWindow::on_txtPlainText_textChanged()
{
    const QString& text = ui->txtPlainText->toPlainText();
    ui->lblPlainText->setText(QString("Plaintext  [ IoC = %1 ]").arg(SuperFish::ioc(
                                                                         SuperFish::normalize(text.toStdString()))));
    updateHistogram(histPlainText, text, axisPlainText);
    if (ui->rdoEncrypt->isChecked())
    {
        processText();
    }
}

void MainWindow::on_txtCipherText_textChanged()
{
    const QString& text = ui->txtCipherText->toPlainText();
    ui->lblCipherText->setText(QString("Ciphertext  [ IoC = %1 ]").arg(SuperFish::ioc(
                                                                           SuperFish::normalize(text.toStdString()))));
    updateHistogram(histCipherText, ui->txtCipherText->toPlainText(), axisCipherText);
    if (ui->rdoDecrypt->isChecked())
    {
        processText();
    }
}

void MainWindow::on_txtKey_textChanged(const QString&)
{
    processText();
}

void MainWindow::processText()
{
    const int cursorPos = ui->txtKey->cursorPosition();
    QString filteredText;
    Q_FOREACH(const auto& c, ui->txtKey->text())
    {
        if (c.isLower())
        {
            filteredText += c;
        }
    }
    if (filteredText != ui->txtKey->text())
    {
        ui->txtKey->setText(filteredText);
        ui->txtKey->setCursorPosition(cursorPos - 1);
    }
    if (ui->txtKey->text().length() != SuperFish::minKeyLength)
    {
        ui->lblKey->setText("Key too short!");
        ui->lblKey->setStyleSheet("QLabel { color: red; }");
        return;
    }
    else
    {
        ui->lblKey->setText("OK");
        ui->lblKey->setStyleSheet("QLabel { color: green; }");
    }
    const string& longKey = SuperFish::expandKey(ui->txtKey->text().toStdString());
    if (ui->rdoEncrypt->isChecked())
    {
        const string& normalized = SuperFish::normalize(ui->txtPlainText->toPlainText().toStdString());
        ui->txtCipherText->setPlainText(QString::fromStdString(SuperFish::encrypt(normalized, longKey)));
    }
    else if (ui->rdoDecrypt->isChecked())
    {
        const string& normalized = SuperFish::normalize(ui->txtCipherText->toPlainText().toStdString());
        ui->txtPlainText->setPlainText(QString::fromStdString(SuperFish::decrypt(normalized, longKey)));
    }
}

void MainWindow::openRotors()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(QString::fromStdString(SuperFish::rotorsFile)));
}

void MainWindow::helpAbout()
{
    QMessageBox::about(this, "About", "SuperFish ver. " + AppVersion + " is polyalphabetic substitution cipher."
                                                                       "\n\nThis piece of software is distributed under terms of GNU GPLv3 license."
                                                                       "\n\nCopyright (c) 2024 by LK.");
}

void MainWindow::helpAboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_pshShowKey_clicked()
{
    if (ui->txtKey->echoMode() == QLineEdit::Password)
    {
        ui->pshShowKey->setText("Hide");
        ui->txtKey->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        ui->pshShowKey->setText("Show");
        ui->txtKey->setEchoMode(QLineEdit::Password);
    }
}

void MainWindow::on_pshPlainTextCopy_clicked()
{
    QGuiApplication::clipboard()->setText(ui->txtPlainText->toPlainText());
}


void MainWindow::on_pshPlainTextPaste_clicked()
{
    ui->txtPlainText->setPlainText(QGuiApplication::clipboard()->text());
}

void MainWindow::on_pshPlainTextOpen_clicked()
{
    const QString& text = getSelectedFile();
    if (text.isEmpty())
    {
        QMessageBox::critical(this, "Error", "Selected file is empty!");
        return;
    }
    ui->txtPlainText->setPlainText(text);
}

void MainWindow::on_pshPlaintTextSave_clicked()
{
    saveSelectedFile(ui->txtPlainText->toPlainText());
}

void MainWindow::on_pshCipherTextCopy_clicked()
{
    QGuiApplication::clipboard()->setText(ui->txtCipherText->toPlainText());
}

void MainWindow::on_pshCipherTextPaste_clicked()
{
    ui->txtCipherText->setPlainText(QGuiApplication::clipboard()->text());
}

void MainWindow::on_pshCipherTextOpen_clicked()
{
    const QString& text = getSelectedFile();
    if (text.isEmpty())
    {
        QMessageBox::critical(this, "Error", "Selected file is empty!");
        return;
    }
    ui->txtCipherText->setPlainText(text);
}

void MainWindow::on_pshCipherTextSave_clicked()
{
    saveSelectedFile(ui->txtCipherText->toPlainText());
}

void MainWindow::on_rdoEncrypt_toggled(bool checked)
{
    ui->txtPlainText->setReadOnly(!checked);
    ui->pshPlainTextOpen->setEnabled(checked);
    ui->pshPlainTextPaste->setEnabled(checked);
    ui->txtCipherText->setReadOnly(checked);
    ui->pshCipherTextOpen->setEnabled(!checked);
    ui->pshCipherTextPaste->setEnabled(!checked);
}

void MainWindow::on_rdoDecrypt_toggled(bool checked)
{
    on_rdoEncrypt_toggled(!checked);
}
