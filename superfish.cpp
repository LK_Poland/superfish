#include "superfish.h"

const string& SuperFish::rotorsFile = "rotors.txt";

vector<string> SuperFish::rotorInit =
{
    "bailkhtcyzsnouxmvepdfjrwgq",
    "biaeyxzckwurlsjgfnodhvpqtm",
    "bieaxmhkjvrwnsgqzycupldtof",
    "biexakcofnlzvmhdqrpwtugyjs",
    "ibexkaymfquczwgonlhrvjptsd",
    "ebixkyacusldwvmofqjngpzthr",
    "eixbkycadjlfnhrvgtqzpsouwm",
    "xbkieycdahvtwqsmgzjlpnrufo",
    "bkxyeicdhanrzowpltusmqjfgv",
    "becykixdhnaowfjvsqturzpmlg",
    "dxckibyehnoapuqtzsmrgljvfw",
    "hdkxbyecinopafqrwmltvzusgj",
    "ncxdhibkeyopfawtljgurqmsvz",
    "oixcdbnekyhpfwajrsglqumzvt",
    "nkibxcyodphefwjavgltzrqusm",
    "bphndyxfekiocwjvaulmzqsrtg",
    "fnxcpoekwibhdyjvuazlgqsmtr",
    "njydobehcxwkpifvuzatsmqrlg",
    "ypciejfhndxbwovkuztarmsqlg",
    "deipjcnoyubkhxfwvztralqmsg",
    "ywdiknfchouzxbpevjtrlagmsq",
    "twpjcxukbhvdfziyeonrlgaqms",
    "rkywcenxofudthbvzijplgqasm",
    "jtnrhopzuwidfbyxkelvcgqsam",
    "kmbpsqgnwutrhicjylafdzeovx",
    "dkwofpciylhtnmgvsxjeurzqba"
};

class Rotor
{
    private:
        char slot[SuperFish::alphabetSize];

    public:
        Rotor(const std::string& value)
        {
            if (value.length() != SuperFish::alphabetSize)
            {
                cout << "Initialization string must be " << SuperFish::alphabetSize << " characters long!" << endl;
                return;
            }
            for (int i = 0 ; i < SuperFish::alphabetSize ; ++i)
            {
                slot[i] = value[i];
            }
        }

        void shiftBy(const int value)
        {
            if (value > 0)
            {
                for (int ctr = 0 ; ctr < abs(value) ; ++ctr)
                {
                    const char first = slot[0];
                    for (int i = 1 ; i < SuperFish::alphabetSize ; ++i)
                    {
                        slot[i - 1] = slot[i];
                    }
                    slot[SuperFish::alphabetSize - 1] = first;
                }
            }
            else
            {
                for (int ctr = 0 ; ctr < abs(value) ; ++ctr)
                {
                    const char last = slot[SuperFish::alphabetSize - 1];
                    for (int i = SuperFish::alphabetSize - 2 ; i >= 0 ; --i)
                    {
                        slot[i + 1] = slot[i];
                    }
                    slot[0] = last;
                }
            }
        }

        char valueFor(const char key) const
        {
            return slot[key - 'a'];
        }

        char keyFor(const char value) const
        {
            for (int i = 0 ; i < SuperFish::alphabetSize ; ++i)
            {
                if (slot[i] == value)
                {
                    return 'a' + i;
                }
            }
            return 'z';
        }

        string toString() const
        {
            return string((char*)&slot, SuperFish::alphabetSize);
        }

        void print() const
        {
            cout << toString() << endl;
        }
};

typedef shared_ptr<Rotor> RotorPtr;

static bool createRotors(RotorPtr* rotor, const string& key)
{
    cout << "Creating " << SuperFish::rotorInit.size() << " rotors..." << endl;
    if ((int)key.length() < SuperFish::minKeyLength)
    {
        cout << "Key must have at least " << SuperFish::minKeyLength << " characters!" << endl;
        return false;
    }
    const string& longKey = SuperFish::expandKey(key);
    deque<int> index;
    for (uint32_t i = 0 ; i != SuperFish::rotorInit.size() ; ++i)
    {
        index.push_back(i);
    }
    for (uint32_t i = 0 ; i != SuperFish::rotorInit.size() ; ++i)
    {
        const int ii = longKey[i] % index.size();
        rotor[i] = make_shared<Rotor>(SuperFish::rotorInit[index[ii]]);
        //cout << "Rotor #" << i + 1 << ": " << rotor[i]->toString() << endl;
        rotor[i]->shiftBy(longKey[i]);
        index.erase(index.begin() + ii);
    }
    return true;
}

string SuperFish::encrypt(const string& text, const string& key)
{
    cout << __PRETTY_FUNCTION__ << endl;

    RotorPtr rotor[SuperFish::rotorInit.size()];
    if (!createRotors(rotor, key))
    {
        return "";
    }

    string cipher = text;

    for (char& b : cipher)
    {
        for (uint32_t i = 0 ; i != SuperFish::rotorInit.size() ; ++i)
        {
            b = rotor[i]->valueFor(b);
            rotor[i]->shiftBy(i % 2 ? 1 : -1);
        }
    }

    return cipher;
}

string SuperFish::decrypt(const string& text, const string& key)
{
    cout << __PRETTY_FUNCTION__ << endl;

    RotorPtr rotor[SuperFish::rotorInit.size()];
    if (!createRotors(rotor, key))
    {
        return "";
    }

    string cipher = text;

    for (char& b : cipher)
    {
        for (int i = SuperFish::rotorInit.size() - 1 ; i >= 0 ; --i)
        {
            b = rotor[i]->keyFor(b);
            rotor[i]->shiftBy(i % 2 ? 1 : -1);
        }
    }

    return cipher;
}

int SuperFish::addRotorsFromFile(const string &file)
{
    int count = 0;
    ifstream fin(file, ifstream::in);
    while (fin.good())
    {
        string line;
        std::getline(fin, line);
        if (line.length() == alphabetSize)
        {
            bool valid = true;
            for (const auto& ch : line)
            {
                if (ch < 'a' || ch > 'z')
                {
                    valid = false;
                    break;
                }
            }
            if (valid)
            {
                rotorInit.push_back(line);
                ++count;
            }
        }
    }
    return count;
}

string SuperFish::expandKey(const string &key)
{
    QCryptographicHash hash(QCryptographicHash::Keccak_512);
    string longKey = key;
    while (true)
    {
        hash.addData(longKey.data(), longKey.length());
        Q_FOREACH(const char& ch, hash.result())
        {
            longKey += 'a' + (abs(ch) % alphabetSize);
            if (longKey.length() >= SuperFish::rotorInit.size())
            {
                return longKey;
            }
        }
        hash.reset();
    }
}

int SuperFish::keyLengthInBits()
{
    return rotorInit.size() * 4.7;
}

vector<unsigned int> SuperFish::histogram(const std::string& text)
{
    vector<unsigned int> letter(alphabetSize, 0);
    for (const char c : text)
    {
        ++letter[c - 'a'];
    }
    return letter;
}

double SuperFish::ioc(const std::string& text)
{
    const vector<unsigned int>& letter = histogram(text);
    double sum = 0;
    for (const unsigned int c : letter)
    {
        sum += c * (c - 1);
    }
    return sum / (text.size() * (text.size() - 1));
}

string SuperFish::normalize(const std::string& text)
{
    string out;
    for (char c : text)
    {
        if (c >= 'a' && c <= 'z')
        {
            out += c;
        }
        else if (c >= 'A' && c <= 'Z')
        {
            c -= 'A' - 'a';
            out += c;
        }
    }
    return out;
}
