#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QClipboard>
#include <QDebug>
#include <QDesktopServices>
#include <QHBoxLayout>
#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QTextStream>

#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>

#include "superfish.h"

using namespace QtCharts;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private slots:
        void on_txtPlainText_textChanged();
        void processText();
        void openRotors();
        void helpAbout();
        void helpAboutQt();

        QString readFile(const QString &path);
        void writeFile(const QString &path, const QString &text);
        QString getSelectedFile();
        void saveSelectedFile(const QString &text);

        void on_txtKey_textChanged(const QString &arg1);
        void on_pshShowKey_clicked();
        void on_pshPlainTextCopy_clicked();
        void on_pshPlainTextPaste_clicked();
        void on_pshPlainTextOpen_clicked();
        void on_pshPlaintTextSave_clicked();
        void on_pshCipherTextCopy_clicked();
        void on_pshCipherTextPaste_clicked();
        void on_pshCipherTextOpen_clicked();
        void on_pshCipherTextSave_clicked();
        void on_txtCipherText_textChanged();
        void on_rdoEncrypt_toggled(bool checked);
        void on_rdoDecrypt_toggled(bool checked);

        QChart *createHistogram(QHBoxLayout *layout, const QString &title, const QString &text, QValueAxis **yAxis);
        void updateHistogram(QChart *chart, const QString& text, QValueAxis *yAxis);

    private:
        Ui::MainWindow *ui;
        QChart *histPlainText = nullptr, *histCipherText = nullptr;
        QValueAxis *axisPlainText = nullptr, *axisCipherText = nullptr;
};

#endif // MAINWINDOW_H
