#ifndef ENCRYPT_H
#define ENCRYPT_H

#include <QCryptographicHash>

#include <cmath>
#include <deque>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class SuperFish final
{
    public:
        static string encrypt(const string &text, const string &key);
        static string decrypt(const string &text, const string &key);
        static int addRotorsFromFile(const string &file = rotorsFile);
        static string expandKey(const string &key);
        static int keyLengthInBits();
        static vector<unsigned int> histogram(const std::string &encrypted);
        static double ioc(const std::string &encrypted);
        static string normalize(const std::string &text);
        static const int alphabetSize = 'z' - 'a' + 1;
        static const int minKeyLength = 18;
        static const string& rotorsFile;
        static vector<string> rotorInit;
};

#endif // ENCRYPT_H
